import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div class="article">
                <h3 class="paneTitle">Overview</h3>
                <div class="pane">
                    <p>The ViCue Video Codec Analyzer (VCA) is a graphical coded video bitstream analysis tool, supporting the following coding standards:</p>
                    <ul>
                        <li>HEVC: (ISO/IEC 23008-2 MPEG-H Part 2 or ITU-T H.265) , 8/10-bit</li>
                        <li>HEVC: RExt extension, 8/10/12-bit, 4:0:0/4:2:0/4:2:2/4:4:4</li>
                        <li>HEVC: SCC extension, conform to HM + SCM 8.6 in reference code</li>
                        <li>HEVC Scalable/Multiview Extension</li>
                        <li>AV1, SHA-1 (Version 1.0): d14c5bb4f336ef1842046089849dee4a301fbbf0</li>
                        <li>Google’s VP9, profiles 0,1,2,3, 4:2:0/4:2:2/4:4:0/4:4:4, 8/10/12-bit</li>
                        <li>AVC: (H.264/AVC, ISO/IEC 14496-10, MPEG-4 Part 10), except SVC/MVC</li>
                        <li>MPEG2 (ISO/IEC 13818-2 Part 2), 4:2:0/4:2:2, 8-bit</li>
                        <li>MKV container</li>
                        <li>MP4 container</li>
                        <li>MPEG2 TS/PS container</li>
                        <li>AVI container</li>
                        <li>Mpeg Media Transport container (ARIB STD-B60 1.0)</li>
                    </ul>
                    <p>Once a bitstream is loaded, the tool allows the user to inspect each major step of the decode process visually and numerically, and the structure of the coded image can be explored. This data can be used as a visual reference when learning about HEVC/VP9/AVC/MPEG2/AV1 or when debugging a particular encoder or decoder.
                    </p>
                </div>
            </div>

      </div>
    );
  }
}

export default App;
